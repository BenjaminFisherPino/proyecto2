#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from window_info import Window_Info
from buscar_archivo import DlgFileChooser
from biopandas.pdb import PandasPdb
import __main__
__main__.pymol_argv=[" pymol ","-qc"] # noqa : E402
import pymol
pymol.finish_launching()

class MainWindow():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("UI/MainWindow.ui")

        interfaz = builder.get_object("MainWindow")
        interfaz.connect("destroy", Gtk.main_quit)
        interfaz.set_title("Analisis proteína")
        interfaz.set_default_size(800,600)

        self.combobox_criterio = builder.get_object("crit_cbb")
        render_text = Gtk.CellRendererText()
        self.combobox_criterio.pack_start(render_text, True)
        criterios_list = Gtk.ListStore(str)
        self.combobox_criterio.add_attribute(render_text, "text", 0)
        self.combobox_criterio.connect("changed", self.on_cbb_crit_select)
        criterios = ["ATOM","HETATM","ANISOU","OTHERS"]
        for i in criterios:
            criterios_list.append([i])
        self.combobox_criterio.set_model(criterios_list)

        self.imag_protein = builder.get_object("protein_imag")
        self.list_protein = builder.get_object("list_protein")

        self.id_protein = builder.get_object("label_id_proteina")
        self.label_lines = builder.get_object("label_miline")

        self.btn_abrir_p = builder.get_object("btn_abrir")
        self.btn_abrir_p.connect("clicked", self.on_btn_open_save_file, "open")

        self.btn_guardar_p = builder.get_object("btn_guardar")
        self.btn_guardar_p.connect("clicked", self.on_btn_open_save_file, "save")

        self.btn_acerca_de = builder.get_object("btn_info")
        self.btn_acerca_de.connect("clicked", self.on_btn_info)

        interfaz.show_all()

    def on_btn_open_save_file(self, btn=None, opt=None):
        buscar_archivo = DlgFileChooser(option=opt)
        dlgfile = buscar_archivo.dlgfile

        if opt == "open":
            filter_pdb = Gtk.FileFilter()
            filter_pdb.set_name("pdb files")
            filter_pdb.add_mime_type("application/x-aportisdoc")
            dlgfile.add_filter(filter_pdb)

        response = dlgfile.run()

        if response == Gtk.ResponseType.OK:
            self.update(dlgfile.get_filename())
            self.rutita = dlgfile.get_filename()
            self.lista_name_ruta = self.rutita_and_nombrecito()

        elif response == Gtk.ResponseType.ACCEPT:
            self.on_btn_save2_file(dlgfile.get_filename())

        dlgfile.destroy()
    
    def rutita_and_nombrecito(self):
        ver_name = os.path.split(self.rutita)
        return ver_name
            
    def update(self, ruta):
        self.subir = PandasPdb()
        self.subir.read_pdb(ruta)
        self.label_lines.set_text("Detalles del archivo: \n%s"% self.subir.pdb_text[:1000])

    def on_btn_save2_file(self, pdb_file):
        if self.valor_combobox == 'ATOM':
            with open(pdb_file, 'w') as newfile:
                self.subir.to_pdb(path = pdb_file,
                                  records = ['ATOM'],
                                  gz = False,
                                  append_newline = True)
        if self.valor_combobox == "HETATM":
            with open(pdb_file, 'w') as newfile:
                self.subir.to_pdb(path = pdb_file,
                                  records = ["HETATM"],
                                  gz = False,
                                  append_newline = True)
        if self.valor_combobox == "ANISOU":
            with open(pdb_file, 'w') as newfile:
                self.subir.to_pdb(path = pdb_file,
                                  records = ["ANISOU"],
                                  gz = False,
                                  append_newline = True)
        if self.valor_combobox == "OTHERS":
            with open(pdb_file, 'w') as newfile:
                self.subir.to_pdb(path = pdb_file,
                                  records = ["OTHERS"],
                                  gz = False,
                                  append_newline = True)

    def on_btn_info(self, btn=None):
        w_inf = Window_Info()
        resp = w_inf.window_info.run()

        if resp == Gtk.ResponseType.CANCEL:
            w_inf.window_info.destroy()

    def on_cbb_crit_select(self, cmb=None):
        rutita = str(self.lista_name_ruta[0])
        self.nombre = str(self.lista_name_ruta[1])
        self.rutitamaestra = rutita + "/" + str(self.nombre)

        it = self.combobox_criterio.get_active_iter()
        modelo = self.combobox_criterio.get_model()
        self.valor_combobox = modelo[it][0]

        if self.valor_combobox == "ATOM":
            databla = self.datas_b()
        elif self.valor_combobox == "HETATM":
            databla = self.datas_b()
        elif self.valor_combobox == "ANISOU":
            databla = self.datas_b()
        elif self.valor_combobox == "OTHERS":
            databla = self.datas_b()

        self.lista = databla
        self.large = len(self.lista.columns)
        self.modelo = Gtk.ListStore(*(self.large*[str]))
        self.list_protein.set_model(self.modelo)
        
        cell = Gtk.CellRendererText()

        for i in range(self.large):
            columns = Gtk.TreeViewColumn(self.lista.columns[i], cell,
                                         text = i)
            self.list_protein.append_column(columns)
        for value in self.lista.values:
            row = [str(x) for x in value]
            self.modelo.append(row)
        ide,ima = self.gen_image()
        imagename = str(ima) + ".png"
        self.imag_protein.set_from_file(imagename)
        self.id_protein.set_text(str(ide))

    def gen_image(self):
        name = self.nombre
        aux = self.nombre.split(".")
        pdb_name = aux[0]
        png_file = "".join([self.rutita])
        pdb_file = self.rutita
        pymol.cmd.load(pdb_file,pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(pdb_name)
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(pdb_name)
        pymol.cmd.png(png_file)
        pymol.cmd.ray()

        return pdb_name, png_file

    def datas_b(self):
        ppdb = PandasPdb()
        ppdb.read_pdb(self.rutitamaestra)
        if self.valor_combobox == "ATOM":
            try:
                true_rute = "./" + str(self.nombre) + "_" + self.valor_combobox + ".pdb"
                ppdb.to_pdb(path = true_rute,
                records = [self.valor_combobox],
                gz = False,
                append_newline = True )
            except:
                pass
        elif self.valor_combobox == "HETATM":
            try:
                true_rute = "./" + str(self.nombre) + "_" + self.valor_combobox + ".pdb"
                ppdb.to_pdb(path = true_rute,
                records = [self.valor_combobox],
                gz = False,
                append_newline = True )
            except:    
                pass
        elif self.valor_combobox == "ANISOU":
            try:
                true_rute = "./" + str(self.nombre) + "_" + self.valor_combobox + ".pdb"
                ppdb.to_pdb(path = true_rute,
                records = [self.valor_combobox],
                gz = False,
                append_newline = True )
            except:
                pass
        elif self.valor_combobox == "OTHERS":
            try:
                true_rute = "./" + str(self.nombre) + "_" + self.valor_combobox + ".pdb"
                ppdb.to_pdb(path = true_rute,
                records = [self.valor_combobox],
                gz = False,
                append_newline = True )
            except:
                pass
        
        return ppdb.df[self.valor_combobox]   
        

if __name__ == "__main__":
    MainWindow()
    Gtk.main()
