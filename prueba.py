#!/ usr/bin/env python
# -* - coding : utf -8 -* -

from biopandas.pdb import PandasPdb
import __main__
__main__.pymol_argv=[" pymol ","-qc"] # noqa : E402
import pymol
pymol.finish_launching()


def gen_image(filename="11gs.pbd",path="archivos_pbd/./"):
    name = filename
    aux = filename.split(".")
    pdb_name = aux[0]
    png_file = "".join([path])
    pdb_file = name
    pymol.cmd.load(filename,name)
    pymol.cmd.disable("all")
    pymol.cmd.enable(name)
    pymol.cmd.hide("all")
    pymol.cmd.show("cartoon")
    pymol.cmd.set("ray_opaque_background", 0)
    pymol.cmd.pretty(name)
    pymol.cmd.png(filename)
    pymol.cmd.ray()

    return pdb_name

def datas_b(criterio):
    ppdb = PandasPdb()
    ppdb.read_pdb("archivos_pdb/11gs.pdb")
    # ppdb.read_pdb("application/x-aportisdoc")
    if criterio == "ATOM":
        try:
            ppdb.to_pdb(path ="./11gs_ATOM.pdb",
            records = [criterio],
            gz = False,
            append_newline = True )
        except:
            pass
    elif criterio == "HETATM":
        try:
            ppdb.to_pdb(path ="./11gs_HETATM.pdb",
            records = [criterio],
            gz = False,
            append_newline = True )
        except:    
            pass
    elif criterio == "ANISOU":
        try:
            ppdb.to_pdb(path ="./11gs_ANISOU.pdb",
            records = [criterio],
            gz = False,
            append_newline = True )
        except:
            pass
    elif criterio == "OTHERS":
        try:
            ppdb.to_pdb(path ="./11gs_OTHERS.pdb",
            records = [criterio],
            gz = False,
            append_newline = True )
        except:
            pass
    
    return ppdb.df[criterio]    
