#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DlgFileChooser():

    def __init__(self, option):
        builder = Gtk.Builder()
        builder.add_from_file("UI/MainWindow.ui")

        self.dlgfile = builder.get_object("GtkDlgArchivos")

        if option == "open":
            self.dlgfile.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     Gtk.STOCK_OPEN,
                                     Gtk.ResponseType.OK)
        if option == "save":
            self.dlgfile.set_action(Gtk.FileChooserAction.SAVE)
            self.dlgfile.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     "Guardar",
                                     Gtk.ResponseType.ACCEPT)

        self.dlgfile.show_all()
